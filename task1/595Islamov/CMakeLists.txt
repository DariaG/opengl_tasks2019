add_compile_definitions(GLM_ENABLE_EXPERIMENTAL)

set(SRC_FILES
        common/Application.cpp
        common/DebugOutput.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp
        revolution.cpp
        )

set(HEADER_FILES
        common/Application.hpp
        common/DebugOutput.h
        common/Camera.hpp
        common/Mesh.hpp
        common/ShaderProgram.hpp
        revolution.hpp
        )

MAKE_OPENGL_TASK(595Islamov 1 "${SRC_FILES}")

if (UNIX)
    target_link_libraries(595Islamov1 stdc++fs)
endif ()