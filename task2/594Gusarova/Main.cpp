#include <Application.hpp>
#include <Mesh.hpp>
#include <LightInfo.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>


class SampleApplication : public Application {
public:
    MeshPtr _relief;
    MeshPtr _marker; //Маркер для источника света

    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;

    //Переменные для управления положением одного источника света
    float _lr = 3.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    LightInfo _light;

    TexturePtr _snow;
    TexturePtr _gravel;
    TexturePtr _ground;
    TexturePtr _grass;
    TexturePtr _mask;

    std::vector<float> _heights;

    GLuint _samplers[5];

    SampleApplication()  { }



    void makeScene() override {
        Application::makeScene();

        _relief = makeRelief(5.0f, 8, 32, 0.5, 10, &_heights, 0);
        int scale = sqrt(_heights.size());
        _cameraMover = std::make_shared<WalkingCameraMover>(scale, 5.0f, _heights);

        _marker = makeSphere(0.1f);

        //Создаем шейдерную программу
        //_shader = std::make_shared<ShaderProgram>("594GusarovaData1/shaderHeight.vert", "594GusarovaData1/shader.frag");
        _shader = std::make_shared<ShaderProgram>("594GusarovaData1/texture.vert", "594GusarovaData1/texture.frag");
        _markerShader = std::make_shared<ShaderProgram>("594GusarovaData1/marker.vert", "594GusarovaData1/marker.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Загрузка и создание текстур
        _snow = loadTexture("594GusarovaData1/snow.jpg");
        _gravel = loadTexture("594GusarovaData1/gravel_3.jpg");
        _ground = loadTexture("594GusarovaData1/sand_2.jpg");
        _grass = loadTexture("594GusarovaData1/grass.jpg");
        _mask = loadTexture("594GusarovaData1/mask_out.png");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(5, _samplers);
        for (int i=0; i<5; i++){
            glSamplerParameteri(_samplers[i], GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glSamplerParameteri(_samplers[i], GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glSamplerParameteri(_samplers[i], GL_TEXTURE_WRAP_S, GL_REPEAT);
            glSamplerParameteri(_samplers[i], GL_TEXTURE_WRAP_T, GL_REPEAT);
        }
    }


    void draw() override {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);



        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _samplers[0]);
        _gravel->bind();
        _shader->setIntUniform("OurTexture1", 0);

        glActiveTexture(GL_TEXTURE1);  //текстурный юнит 1
        glBindSampler(1, _samplers[1]);
        _ground->bind();
        _shader->setIntUniform("OurTexture2", 1);

        glActiveTexture(GL_TEXTURE2);  //текстурный юнит 2
        glBindSampler(2, _samplers[2]);
        _grass->bind();
        _shader->setIntUniform("OurTexture3", 2);

        glActiveTexture(GL_TEXTURE3);  //текстурный юнит 3
        glBindSampler(3, _samplers[3]);
        _snow->bind();
        _shader->setIntUniform("OurTexture4", 3);

        glActiveTexture(GL_TEXTURE4);  //текстурный юнит 3
        glBindSampler(4, _samplers[4]);
        _mask->bind();
        _shader->setIntUniform("OurTexture5", 4);


        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        {
            _shader->setMat4Uniform("modelMatrix", _relief->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(
                    glm::inverse(glm::mat3(_camera.viewMatrix * _relief->modelMatrix()))));
            _relief->draw();
        }

        //Рисуем маркеры для всех источников света
        {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }


};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}