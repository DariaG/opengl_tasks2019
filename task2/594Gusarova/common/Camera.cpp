#include <Camera.hpp>

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>

#include <iostream>

#define GLM_ENABLE_EXPERIMENTAL

void OrbitCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

void OrbitCameraMover::showOrientationParametersImgui() {
    ImGui::Text("r = %.2f, phi = %.2f, theta = %2f", _r, _phiAng, _thetaAng);
}

void OrbitCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        _phiAng -= dx * 0.005;
        _thetaAng += dy * 0.005;

        _thetaAng = glm::clamp(_thetaAng, -glm::pi<double>() * 0.49, glm::pi<double>() * 0.49);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void OrbitCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
    _r += _r * yoffset * 0.05;
}

void OrbitCameraMover::update(GLFWwindow* window, double dt)
{
    double speed = 1.0;

    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        _phiAng -= speed * dt;
        //_r = _r / cos(speed * dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        _phiAng += speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        _thetaAng += speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        _thetaAng -= speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
    {
        _r += _r * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS)
    {
        _r -= _r * dt;
    }

    _thetaAng = glm::clamp(_thetaAng, -glm::pi<double>() * 0.49, glm::pi<double>() * 0.49);

    //-----------------------------------------

    //Вычисляем положение виртуальной камеры в мировой системе координат по формуле сферических координат
    glm::vec3 pos = glm::vec3(glm::cos(_phiAng) * glm::cos(_thetaAng), glm::sin(_phiAng) * glm::cos(_thetaAng), glm::sin(_thetaAng) + 0.5f) * (float)_r;

    //Обновляем матрицу вида
    _camera.viewMatrix = glm::lookAt(pos, glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f));

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}

//=============================================

FreeCameraMover::FreeCameraMover() :
CameraMover(),
_pos(5.0f, 0.0f, 2.5f)
{       
    //Нам нужно как-нибудь посчитать начальную ориентацию камеры
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 2.0f)));
}

void FreeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

void FreeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        //Добавляем небольшой поворот вверх/вниз        
        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
        _rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

        //Добавляем небольшой поворов вокруг вертикальной оси
        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void FreeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void FreeCameraMover::update(GLFWwindow* window, double dt)
{
    float speed = 1.0f;

    //Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
        
    //Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        _pos += forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        _pos -= forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        _pos -= rightDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        _pos += rightDir * speed * static_cast<float>(dt);
    }

    //-----------------------------------------

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);
    
    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}



int calculateSquare(float x, float y, float size, float shift, int scale)
{
    int i = (int)((x + size) / shift);
    int j = (int)((y + size) / shift);
    return j * scale + i;
}

WalkingCameraMover::WalkingCameraMover(int scale, float size, std::vector<float> heights) :
        CameraMover()
{
    _scale = scale;
    _heights = heights;
    _size = size;
    _shift = 2.0 * size / scale;

    int sq = calculateSquare(0.0, 0.0, _size, _shift, _scale);
    int x = sq % _scale;
    int y = sq / _scale;
    _pos = glm::vec3(-_size + x * _shift, -_size + y * _shift, _heights[sq] + 0.1);

    _to_look = glm::vec2(0.0, 1.0);
    int sq_ = calculateSquare(0.0 + _to_look.x, 0.0 + _to_look.y, _size, _shift, _scale);
    int x_ = sq_ % _scale;
    int y_ = sq_ / _scale;

    float he = fmax(_pos.z, _heights[sq_] + 0.1);
    glm::vec3 center = glm::vec3(-_size + (x + x_) * _shift, -_size + (y + y_) * _shift, he);
    _rot = glm::toQuat(glm::lookAt(_pos, center, glm::vec3(0.0, 0.0, 1.0)));
}


void WalkingCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

void WalkingCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{

}

void WalkingCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void WalkingCameraMover::update(GLFWwindow* window, double dt)
{
    float speed = 1.0f;

    float step = speed * static_cast<float>(dt);

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        _pos.x += _to_look.x * step;
        _pos.y += _to_look.y * step;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        _pos.x -= _to_look.x * step;
        _pos.y -= _to_look.y * step;
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        float theta = step;
        float new_x = _to_look.x * glm::cos(theta) -  _to_look.y * glm::sin(theta);
        float new_y = _to_look.x * glm::sin(theta) +  _to_look.y * glm::cos(theta);
        _to_look = glm::vec2(new_x, new_y);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        float theta = step;
        float new_x = _to_look.x * glm::cos(theta) + _to_look.y * glm::sin(theta);
        float new_y = -_to_look.x * glm::sin(theta) +  _to_look.y * glm::cos(theta);
        _to_look = glm::vec2(new_x, new_y);
    }
    int sq = calculateSquare(_pos.x, _pos.y, _size, _shift, _scale);
    int x = sq % _scale;
    int y = sq / _scale;
    _pos = glm::vec3(-_size + x * _shift, -_size + y * _shift, _heights[sq] + 0.1);

    int sq_ = calculateSquare(_pos.x + _to_look.x, _pos.y + _to_look.y, _size, _shift, _scale);
    int x_ = sq_ % _scale;
    int y_ = sq_ / _scale;

    float he = fmax(_pos.z, _heights[sq_] + 0.1);
    glm::vec3 center = glm::vec3(-_size + (x + x_) * _shift, -_size + (y + y_) * _shift, he);
    _rot = glm::toQuat(glm::lookAt(_pos, center, glm::vec3(0.0, 0.0, 1.0)));
    //-----------------------------------------

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}