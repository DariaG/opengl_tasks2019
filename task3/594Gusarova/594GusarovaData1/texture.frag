/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330


uniform sampler2D OurTexture1;
uniform sampler2D OurTexture2;
uniform sampler2D OurTexture3;
uniform sampler2D OurTexture4;
uniform sampler2D OurTexture5;
uniform sampler2DShadow shadowTex;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;

in vec3 vertexPos;
in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
in vec2 texCoordMask;
in vec4 shadowTexCoord; //выходные текстурные координаты для проективное текстуры

out vec4 fragColor; //выходной цвет фрагмента

uniform float bias;

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;

void main()
{
	vec4 shadowSampleCoord = shadowTexCoord;
	shadowSampleCoord.z += bias * shadowSampleCoord.w;
	float visibility = textureProj(shadowTex, shadowSampleCoord); //глубина ближайшего фрагмента в пространстве источника света

	vec3 texture1Color = texture(OurTexture1, texCoord).rgb;
	vec3 texture2Color = texture(OurTexture2, texCoord).rgb;
	vec3 texture3Color = texture(OurTexture3, texCoord).rgb;
	vec3 texture4Color = texture(OurTexture4, texCoord).rgb;
    vec4 mask = texture(OurTexture5, texCoordMask).rgba;

    vec3 diffuseColor = mask.r * texture1Color + mask.g * texture2Color + mask.b * texture3Color + mask.a * texture4Color;

	vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
	vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
	
	vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz); //направление на источник света	

	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

	vec3 color = diffuseColor * (light.La + light.Ld * NdotL * visibility);

	if (NdotL > 0.0)
	{			
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

		float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну				
		blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
		//vec3 ls = light.Ls * (mask[0] * 0.1 + mask[1] * 0.2 + mask[2] * 0.1 + mask[3] * 1.0);
        vec3 ls = light.Ls * (mask.r * 0.1 + mask.g * 0.2 + mask.b * 0.1 + mask.a * 1.0);
		color += ls * Ks * blinnTerm * visibility;
	}

	fragColor = vec4(color, 1.0);
}
