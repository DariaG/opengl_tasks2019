#version 330

uniform sampler2D OurTexture1;
uniform sampler2D OurTexture2;
uniform sampler2D OurTexture3;
uniform sampler2D OurTexture4;
uniform sampler2D OurTexture5;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
in vec2 texCoordMask;

layout(location = 0) out vec3 normal; // "go to GL_COLOR_ATTACHMENT0"
layout(location = 1) out vec3 diffuseColor; // "go to GL_COLOR_ATTACHMENT1"

void main()
{
	vec3 texture1Color = texture(OurTexture1, texCoord).rgb;
	vec3 texture2Color = texture(OurTexture2, texCoord).rgb;
	vec3 texture3Color = texture(OurTexture3, texCoord).rgb;
	vec3 texture4Color = texture(OurTexture4, texCoord).rgb;
	vec4 mask = texture(OurTexture5, texCoordMask).rgba;
	//mask = vec4(0.0, 0.0, 1.0, 0.0);

	diffuseColor = mask.r * texture1Color + mask.g * texture2Color + mask.b * texture3Color + mask.a * texture4Color;

	normal = normalize(normalCamSpace) * 0.5 + 0.5;
}
