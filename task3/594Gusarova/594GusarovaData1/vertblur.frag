#version 330

uniform sampler2D tex;

in vec2 texCoord; //текстурные координаты (интерполированы между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

void main()
{
	vec2 size = textureSize(tex, 0);
    vec2 dx = vec2(0.0, 1.0 / size.y); //шаг по горизонтали
    vec2 sdx = dx;
    vec4 sum = texture(tex, texCoord) * 0.134598;

    //Размытие по вертикали по Гауссу

#if 0 // Choose 0 here to see there is nothing bad in cycles if they are used wisely :)
    sum += (texture(tex, texCoord + sdx) + texture(tex, texCoord - sdx)) * 0.127325;
    sdx += dx;
    sum += (texture(tex, texCoord + sdx) + texture(tex, texCoord - sdx)) * 0.107778;
    sdx += dx;
    sum += (texture(tex, texCoord + sdx) + texture(tex, texCoord - sdx)) * 0.081638;
    sdx += dx;
    sum += (texture(tex, texCoord + sdx) + texture(tex, texCoord - sdx)) * 0.055335;
    sdx += dx;
    sum += (texture(tex, texCoord + sdx) + texture(tex, texCoord - sdx)) * 0.033562;
    sdx += dx;
    sum += (texture(tex, texCoord + sdx) + texture(tex, texCoord - sdx)) * 0.018216;
    sdx += dx;
    sum += (texture(tex, texCoord + sdx) + texture(tex, texCoord - sdx)) * 0.008847;
    sdx += dx;
#else
    const float coeffs[7] = float[7](0.127325f, 0.107778f, 0.081638f, 0.055335f, 0.033562f, 0.018216f, 0.008847f);
    for (int i = 0; i < 7; i++) {
        sum += (texture(tex, texCoord + sdx) + texture(tex, texCoord - sdx)) * coeffs[i];
        sdx += dx;
    }
#endif

    fragColor = sum;
}
