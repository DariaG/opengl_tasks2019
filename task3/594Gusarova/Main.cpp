#include <Application.hpp>
#include <LightInfo.hpp>
#include <Framebuffer.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include <iostream>
#include <sstream>
#include <vector>


namespace {
    float frand() {
        return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
    }
}

class SampleApplication : public Application
{
public:
    MeshPtr _relief;
    MeshPtr _tree;
    MeshPtr _backgroundCube; //Для фона
    //MeshPtr _bunny;
    //MeshPtr _ground;

    MeshPtr _quad;

    ShaderProgramPtr _skyboxShader;
    //Идентификатор шейдерной программы
    ShaderProgramPtr _quadDepthShader;
    ShaderProgramPtr _quadColorShader;
    ShaderProgramPtr _renderToShadowMapShader;
    ShaderProgramPtr _renderToShadowMapShaderTree;
    ShaderProgramPtr _renderToGBufferShader;
    ShaderProgramPtr _renderToGBufferShaderTree;
    ShaderProgramPtr _renderDeferredShader;
    ShaderProgramPtr _brightShader;
    ShaderProgramPtr _horizBlurShader;
    ShaderProgramPtr _vertBlurShader;
    ShaderProgramPtr _toneMappingShader;

    //Переменные для управления положением одного источника света
    float _lr = 10.0;
    float _phi = 0.0;
    float _theta = 0.48;

    float _lightIntensity = 1.0;
    LightInfo _light;
    CameraInfo _lightCamera;

    TexturePtr _brickTex;

    GLuint _sampler;
    GLuint _repeatSampler;
    GLuint _repeatSamplerTree;
    GLuint _cubeTexSampler;
    GLuint _depthSampler;

    bool _applyEffect = true;

    bool _showGBufferDebug = false;
    bool _showShadowDebug = false;
    bool _showDeferredDebug = false;
    bool _showBloomDebug = false;

    float _exposure = 1.0f; //Параметр алгоритма ToneMapping
    float _brightnessThreshold = 1.0f; // Порог яркости для применения размытия.
    float _bloomFactor = 5.0f; // Фактор учёта заблуренной яркости.

    bool _useBias = true;
    float _bias = 0.0f;

    FramebufferPtr _gbufferFB;
    TexturePtr _depthTex;
    TexturePtr _normalsTex;
    TexturePtr _diffuseTex;

    FramebufferPtr _shadowFB;
    TexturePtr _shadowTex;

    FramebufferPtr _deferredFB;
    TexturePtr _deferredTex;

    FramebufferPtr _brightFB;
    TexturePtr _brightTex;

    FramebufferPtr _horizBlurFB;
    TexturePtr _horizBlurTex;

    FramebufferPtr _vertBlurFB;
    TexturePtr _vertBlurTex;

    FramebufferPtr _toneMappingFB;
    TexturePtr _toneMappingTex;

    //Старые размеры экрана
    int _oldWidth = 1024;
    int _oldHeight = 1024;

    std::vector<float> _heights;
    std::vector<glm::vec3> _normals;

    TexturePtr _snow;
    TexturePtr _gravel;
    TexturePtr _ground;
    TexturePtr _grass;
    TexturePtr _mask;
    TexturePtr _cubeTex;
    TexturePtr _leavesTex;
    TexturePtr _barkTex;

    int cpuLoadSize = 100000;

    const unsigned int K = 20; //Количество инстансов

    std::vector<glm::vec3> _positionsVec3;
    DataBufferPtr _bufVec3;
    TexturePtr _bufferTex;

    void initFramebuffers()
    {
        //Создаем фреймбуфер для рендеринга в G-буфер
        _gbufferFB = std::make_shared<Framebuffer>(1024, 1024);

        _normalsTex = _gbufferFB->addBuffer(GL_RGB16F, GL_COLOR_ATTACHMENT0);
        _diffuseTex = _gbufferFB->addBuffer(GL_RGB8, GL_COLOR_ATTACHMENT1);
        _depthTex = _gbufferFB->addBuffer(GL_DEPTH_COMPONENT16, GL_DEPTH_ATTACHMENT);

        _gbufferFB->initDrawBuffers();

        if (!_gbufferFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        //=========================================================
        //Создаем фреймбуфер для рендеринга в теневую карту

        _shadowFB = std::make_shared<Framebuffer>(1024, 1024);

        _shadowTex = _shadowFB->addBuffer(GL_DEPTH_COMPONENT16, GL_DEPTH_ATTACHMENT);

        _shadowFB->initDrawBuffers();

        if (!_shadowFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        //=========================================================
        //Создаем фреймбуфер для результатов расчета освещения

        _deferredFB = std::make_shared<Framebuffer>(1024, 1024);

        _deferredTex = _deferredFB->addBuffer(GL_RGB32F, GL_COLOR_ATTACHMENT0);

        _deferredFB->initDrawBuffers();

        if (!_deferredFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        //=========================================================
        //Фреймбуфер с текстурой, куда будут записаны только самые яркие фрагменты

        _brightFB = std::make_shared<Framebuffer>(512, 512); //В 2 раза меньше

        _brightTex = _brightFB->addBuffer(GL_RGB32F, GL_COLOR_ATTACHMENT0);

        _brightFB->initDrawBuffers();

        if (!_brightFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        //=========================================================

        _horizBlurFB = std::make_shared<Framebuffer>(512, 512); //В 2 раза меньше

        _horizBlurTex = _horizBlurFB->addBuffer(GL_RGB32F, GL_COLOR_ATTACHMENT0);

        _horizBlurFB->initDrawBuffers();

        if (!_horizBlurFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        //=========================================================

        _vertBlurFB = std::make_shared<Framebuffer>(512, 512); //В 2 раза меньше

        _vertBlurTex = _vertBlurFB->addBuffer(GL_RGB32F, GL_COLOR_ATTACHMENT0);

        _vertBlurFB->initDrawBuffers();

        if (!_vertBlurFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        //=========================================================

        _toneMappingFB = std::make_shared<Framebuffer>(1024, 1024);

        _toneMappingTex = _toneMappingFB->addBuffer(GL_RGB8, GL_COLOR_ATTACHMENT0);

        _toneMappingFB->initDrawBuffers();

        if (!_toneMappingFB->valid())
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }
    }

    void cpuLoad(size_t work) {
        for (size_t i = 0; i < work; i++) {
            frand();
        }
    }

    void makeScene() override
    {
        Application::makeScene();

        //=========================================================
        //Создание и загрузка мешей
        _relief = makeRelief(5.0f, 8, 32, 0.5, 10, &_heights, &_normals, 0);
        _tree = makeTree(0.7f, 0.07f, 0.3f);
        _quad = makeScreenAlignedQuad();
        _backgroundCube = makeCube(10.0f);

        srand((int)(glfwGetTime() * 1000));

        const float size = 7.0f;
        for (unsigned int i = 0; i < K; i++)
        {
            _positionsVec3.push_back(glm::vec3(frand() * size - 0.5 * size, frand() * size - 0.5 * size, 0.0));
        }

        //Создаем буферы без выравнивания (_bufVec3)
        _bufVec3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        _bufVec3->setData(_positionsVec3.size() * sizeof(float) * 3, _positionsVec3.data());

        //Создаем текстурный буфер и привязываем к нему буфер без выравнивания
        _bufferTex = std::make_shared<Texture>(GL_TEXTURE_BUFFER);
        _bufferTex->bind();
        glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F_ARB, _bufVec3->id());
        _bufferTex->unbind();

        //=========================================================
        //Инициализация шейдеров
        _skyboxShader = std::make_shared<ShaderProgram>("594GusarovaData1/skybox.vert", "594GusarovaData1/skybox.frag");
        _quadDepthShader = std::make_shared<ShaderProgram>("594GusarovaData1/quadDepth.vert", "594GusarovaData1/quadDepth.frag");
        _quadColorShader = std::make_shared<ShaderProgram>("594GusarovaData1/quadColor.vert", "594GusarovaData1/quadColor.frag");
        _renderToShadowMapShader = std::make_shared<ShaderProgram>("594GusarovaData1/toshadow.vert", "594GusarovaData1/toshadow.frag");
        _renderToShadowMapShaderTree = std::make_shared<ShaderProgram>("594GusarovaData1/toshadowtree.vert", "594GusarovaData1/toshadowtree.frag");
        _renderToGBufferShader = std::make_shared<ShaderProgram>("594GusarovaData1/togbuffer.vert", "594GusarovaData1/togbuffer.frag");
        _renderToGBufferShaderTree = std::make_shared<ShaderProgram>("594GusarovaData1/togbuffer_tree.vert", "594GusarovaData1/togbuffer_tree.frag");
        _renderDeferredShader = std::make_shared<ShaderProgram>("594GusarovaData1/quadColor.vert", "594GusarovaData1/deferred.frag");
        _brightShader = std::make_shared<ShaderProgram>("594GusarovaData1/quadColor.vert", "594GusarovaData1/bright.frag");
        _horizBlurShader = std::make_shared<ShaderProgram>("594GusarovaData1/quadColor.vert", "594GusarovaData1/horizblur.frag");
        _vertBlurShader = std::make_shared<ShaderProgram>("594GusarovaData1/quadColor.vert", "594GusarovaData1/vertblur.frag");
        _toneMappingShader = std::make_shared<ShaderProgram>("594GusarovaData1/quadColor.vert", "594GusarovaData1/tonemapping.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Загрузка и создание текстур
        _brickTex = loadTexture("594GusarovaData1/brick.jpg", SRGB::YES); //sRGB
        _snow = loadTexture("594GusarovaData1/snow.jpg", SRGB::YES);
        _gravel = loadTexture("594GusarovaData1/gravel_3.jpg", SRGB::YES);
        _ground = loadTexture("594GusarovaData1/sand_2.jpg", SRGB::YES);
        _grass = loadTexture("594GusarovaData1/grass.jpg", SRGB::YES);
        _mask = loadTexture("594GusarovaData1/mask_out.png");
        _leavesTex = loadTexture("594GusarovaData1/grass.jpg", SRGB::YES);
        _barkTex = loadTexture("594GusarovaData1/bark.jpg", SRGB::YES);
        _cubeTex = loadCubeTexture("594GusarovaData1/ely_cloudtop");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glGenSamplers(1, &_repeatSampler);

        glSamplerParameteri(_repeatSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_repeatSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_repeatSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_repeatSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_repeatSamplerTree);

        glSamplerParameteri(_repeatSamplerTree, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_repeatSamplerTree, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_repeatSamplerTree, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_repeatSamplerTree, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_cubeTexSampler);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);


        GLfloat border[] = { 1.0f, 0.0f, 0.0f, 1.0f };

        glGenSamplers(1, &_depthSampler);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        glSamplerParameterfv(_depthSampler, GL_TEXTURE_BORDER_COLOR, border);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

        //=========================================================
        glfwGetFramebufferSize(_window, &_oldWidth, &_oldHeight);

        //=========================================================
        //Инициализация фреймбуфера для рендера теневой карты

        initFramebuffers();
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());

                ImGui::SliderFloat("intensity", &_lightIntensity, 0.0f, 5.0f);
            }
            ImGui::Checkbox("Use bias", &_useBias);
            if (_useBias)
                ImGui::DragFloat("Bias", &_bias, 1e-6f, -1e-3f, 1e-3f, "%.6f");

            ImGui::Checkbox("Apply HDR", &_applyEffect);

            ImGui::SliderFloat("Brightness threshold", &_brightnessThreshold, 0.01f, 10.0f);
            ImGui::SliderFloat("Bloom factor", &_bloomFactor, 0.1f, 10.0f);
            ImGui::SliderFloat("Exposure", &_exposure, 0.01f, 10.0f);

            ImGui::Checkbox("Show G-buffer debug", &_showGBufferDebug);
            ImGui::Checkbox("Show shadow debug", &_showShadowDebug);
            ImGui::Checkbox("Show deferred debug", &_showDeferredDebug);
            ImGui::Checkbox("Show bloom debug", &_showBloomDebug);
        }
        ImGui::End();
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_1)
            {
                _useBias = !_useBias;
            } else if (key == GLFW_KEY_2)
            {
                _applyEffect = !_applyEffect;
            }
            else if (key == GLFW_KEY_3)
            {
                _showGBufferDebug = !_showGBufferDebug;
            }
            else if (key == GLFW_KEY_4)
            {
                _showShadowDebug = !_showShadowDebug;
            }
            else if (key == GLFW_KEY_5)
            {
                _showDeferredDebug = !_showDeferredDebug;
            }
            else if (key == GLFW_KEY_6)
            {
                _showBloomDebug = !_showBloomDebug;
            }
        }
    }

    void update()
    {
        Application::update();

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;
        _lightCamera.viewMatrix = glm::lookAt(_light.position, glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        _lightCamera.projMatrix = glm::perspective(glm::radians(60.0f), 1.0f, 0.1f, 30.f);

        //Если размер окна изменился, то изменяем размеры фреймбуферов - перевыделяем память под текстуры

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        if (width != _oldWidth || height != _oldHeight)
        {
            _gbufferFB->resize(width, height);
            _deferredFB->resize(width, height);

            _brightFB->resize(width / 2, height / 2);
            _horizBlurFB->resize(width / 2, height / 2);
            _vertBlurFB->resize(width / 2, height / 2);
            _toneMappingFB->resize(width, height);

            _oldWidth = width;
            _oldHeight = height;
        }
    }

    void draw() override
    {
        //Рендерим геометрию сцены в G-буфер
        drawToGBuffer(_gbufferFB, _camera);

        //Рендерим геометрию сцены в теневую карту с позиции источника света
        drawToShadowMap(_shadowFB, _renderToShadowMapShader, _renderToShadowMapShaderTree, _lightCamera);

        //Выполняем отложенное освещение, заодно накладывает тени, а результат записываем в текстуру
        drawDeferred(_deferredFB, _renderDeferredShader, _camera, _lightCamera);

        //Получаем текстуру с яркими областями
        drawProcessTexture(_brightFB, _brightShader, _deferredTex);

        //Выполняем размытие текстуры с яркими областями
        drawProcessTexture(_horizBlurFB, _horizBlurShader, _brightTex);
        drawProcessTexture(_vertBlurFB, _vertBlurShader, _horizBlurTex);

        if (_applyEffect)
        {
            drawToneMapping(_toneMappingFB, _toneMappingShader);
            drawToScreen(_quadColorShader, _toneMappingTex);
        }
        else
        {
            drawToScreen(_quadColorShader, _deferredTex);
        }

        //Отладочный рендер текстур
        drawDebug();
    }

    void drawToGBuffer(const FramebufferPtr& fb, const CameraInfo& camera)
    {
        fb->bind();

        glViewport(0, 0, fb->width(), fb->height());
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        //====== РИСУЕМ ФОН С КУБИЧЕСКОЙ ТЕКСТУРОЙ ======
        {
            _skyboxShader->use();

            glm::vec3 cameraPos = glm::vec3(glm::inverse(camera.viewMatrix)[3]); //Извлекаем из матрицы вида положение виртуальный камеры в мировой системе координат

            _skyboxShader->setVec3Uniform("cameraPos", cameraPos);
            _skyboxShader->setMat4Uniform("viewMatrix", camera.viewMatrix);
            _skyboxShader->setMat4Uniform("projectionMatrix", camera.projMatrix);

            //Для преобразования координат в текстурные координаты нужна специальная матрица
            glm::mat3 textureMatrix = glm::mat3(0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
            _skyboxShader->setMat3Uniform("textureMatrix", textureMatrix);

            glActiveTexture(GL_TEXTURE8);  //текстурный юнит 0
            glBindSampler(8, _cubeTexSampler);
            _cubeTex->bind();
            _skyboxShader->setIntUniform("cubeTex", 8);

            glDepthMask(GL_FALSE); //Отключаем запись в буфер глубины

            _backgroundCube->draw();

            glDepthMask(GL_TRUE); //Включаем обратно запись в буфер глубины
        }
        //====== РИСУЕМ РЕЛЬЕФ ======
        {
            _renderToGBufferShader->use();
            _renderToGBufferShader->setMat4Uniform("viewMatrix", camera.viewMatrix);
            _renderToGBufferShader->setMat4Uniform("projectionMatrix", camera.projMatrix);

            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindSampler(0, _repeatSampler);
            _gravel->bind();
            _renderToGBufferShader->setIntUniform("OurTexture1", 0);

            glActiveTexture(GL_TEXTURE1);  //текстурный юнит 0
            glBindSampler(1, _repeatSampler);
            _ground->bind();
            _renderToGBufferShader->setIntUniform("OurTexture2", 1);

            glActiveTexture(GL_TEXTURE2);  //текстурный юнит 0
            glBindSampler(2, _repeatSampler);
            _grass->bind();
            _renderToGBufferShader->setIntUniform("OurTexture3", 2);

            glActiveTexture(GL_TEXTURE3);  //текстурный юнит 0
            glBindSampler(3, _repeatSampler);
            _snow->bind();
            _renderToGBufferShader->setIntUniform("OurTexture4", 3);

            glActiveTexture(GL_TEXTURE4);  //текстурный юнит 0
            glBindSampler(4, _repeatSampler);
            _mask->bind();
            _renderToGBufferShader->setIntUniform("OurTexture5", 4);

            _renderToGBufferShader->setMat4Uniform("modelMatrix", _relief->modelMatrix());
            _renderToGBufferShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(
                    glm::inverse(glm::mat3(camera.viewMatrix * _relief->modelMatrix()))));

            _relief->draw();
        }
        //====== РИСУЕМ ДЕРЕВЬЯ ======
        {
            _renderToGBufferShaderTree->use();
            _renderToGBufferShaderTree->setMat4Uniform("viewMatrix", camera.viewMatrix);
            _renderToGBufferShaderTree->setMat4Uniform("projectionMatrix", camera.projMatrix);

            glActiveTexture(GL_TEXTURE5);  //текстурный юнит 0
            glBindSampler(5, _repeatSamplerTree);
            _leavesTex->bind();
            _renderToGBufferShaderTree->setIntUniform("LeavesTexture", 5);

            glActiveTexture(GL_TEXTURE6);  //текстурный юнит 0
            glBindSampler(6, _repeatSamplerTree);
            _barkTex->bind();
            _renderToGBufferShaderTree->setIntUniform("BarkTexture", 6);

            glActiveTexture(GL_TEXTURE7);
            _bufferTex->bind();
            _renderToGBufferShaderTree->setIntUniform("texBuf", 7);

            _renderToGBufferShaderTree->setMat4Uniform("modelMatrix", _tree->modelMatrix());
            _renderToGBufferShaderTree->setMat3Uniform("normalToCameraMatrix", glm::transpose(
                    glm::inverse(glm::mat3(camera.viewMatrix * _tree->modelMatrix()))));

            _tree->drawInstanced(_positionsVec3.size());
        }

        glUseProgram(0); //Отключаем шейдер

        fb->unbind(); //Отключаем фреймбуфер
    }

    void drawToShadowMap(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const ShaderProgramPtr& shaderTree, const CameraInfo& lightCamera)
    {
        fb->bind();

        glViewport(0, 0, fb->width(), fb->height());
        glClear(GL_DEPTH_BUFFER_BIT);

        shader->use();
        shader->setMat4Uniform("lightViewMatrix", lightCamera.viewMatrix);
        shader->setMat4Uniform("lightProjectionMatrix", lightCamera.projMatrix);

        glEnable(GL_CULL_FACE);
        glFrontFace(GL_CCW);
        glCullFace(GL_FRONT);


        shader->setMat4Uniform("modelMatrix", _relief->modelMatrix());
        shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(lightCamera.viewMatrix * _relief->modelMatrix()))));

        _relief->draw();

        glDisable(GL_CULL_FACE);

        shaderTree->use();
        shaderTree->setMat4Uniform("lightViewMatrix", lightCamera.viewMatrix);
        shaderTree->setMat4Uniform("lightProjectionMatrix", lightCamera.projMatrix);

        glActiveTexture(GL_TEXTURE0);
        _bufferTex->bind();
        shaderTree->setIntUniform("texBuf", 0);

        glEnable(GL_CULL_FACE);
        glFrontFace(GL_CCW);
        glCullFace(GL_FRONT);


        shaderTree->setMat4Uniform("modelMatrix", _tree->modelMatrix());
        shaderTree->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(lightCamera.viewMatrix * _tree->modelMatrix()))));

        _tree->drawInstanced(_positionsVec3.size());

        glDisable(GL_CULL_FACE);

        glUseProgram(0);

        fb->unbind();
    }

    void drawDeferred(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const CameraInfo& camera, const CameraInfo& lightCamera)
    {
        fb->bind();

        glViewport(0, 0, fb->width(), fb->height());
        glClear(GL_COLOR_BUFFER_BIT);

        shader->use();
        shader->setMat4Uniform("viewMatrixInverse", glm::inverse(camera.viewMatrix));
        shader->setMat4Uniform("projMatrixInverse", glm::inverse(camera.projMatrix));

        glm::vec3 lightPosCamSpace = glm::vec3(camera.viewMatrix * glm::vec4(_light.position, 1.0));

        shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        shader->setVec3Uniform("light.La", _light.ambient * _lightIntensity);
        shader->setVec3Uniform("light.Ld", _light.diffuse * _lightIntensity);
        shader->setVec3Uniform("light.Ls", _light.specular * _lightIntensity);

        shader->setMat4Uniform("lightViewMatrix", lightCamera.viewMatrix);
        shader->setMat4Uniform("lightProjectionMatrix", lightCamera.projMatrix);

        glm::mat4 projScaleBiasMatrix = glm::scale(glm::translate(glm::mat4(1.0), glm::vec3(0.5, 0.5, 0.5)), glm::vec3(0.5, 0.5, 0.5));
        shader->setMat4Uniform("lightScaleBiasMatrix", projScaleBiasMatrix);

        shader->setFloatUniform("bias", _useBias ? _bias : 0.0f);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _normalsTex->bind();
        shader->setIntUniform("normalsTex", 0);

        glActiveTexture(GL_TEXTURE1);  //текстурный юнит 1
        glBindSampler(1, _sampler);
        _diffuseTex->bind();
        shader->setIntUniform("diffuseTex", 1);

        glActiveTexture(GL_TEXTURE2);  //текстурный юнит 2
        glBindSampler(2, _sampler);
        _depthTex->bind();
        shader->setIntUniform("depthTex", 2);

        glActiveTexture(GL_TEXTURE3);  //текстурный юнит 3
        glBindSampler(3, _depthSampler);
        _shadowTex->bind();
        shader->setIntUniform("shadowTex", 3);

        _quad->draw();

        glUseProgram(0);

        fb->unbind();
    }

    void drawProcessTexture(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const TexturePtr& inputTexture)
    {
        fb->bind();

        glViewport(0, 0, fb->width(), fb->height());
        glClear(GL_COLOR_BUFFER_BIT);

        shader->use();

        shader->setFloatUniform("threshold", _brightnessThreshold);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        inputTexture->bind();
        shader->setIntUniform("tex", 0);

        _quad->draw();

        glUseProgram(0);

        fb->unbind();
    }

    void drawToneMapping(const FramebufferPtr& fb, const ShaderProgramPtr& shader)
    {
        fb->bind();

        glViewport(0, 0, fb->width(), fb->height());
        glClear(GL_COLOR_BUFFER_BIT);

        shader->use();

        shader->setFloatUniform("bloomFactor", _bloomFactor);
        shader->setFloatUniform("exposure", _exposure);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _deferredTex->bind();
        shader->setIntUniform("tex", 0);

        glActiveTexture(GL_TEXTURE1);
        glBindSampler(1, _sampler);
        _vertBlurTex->bind();
        shader->setIntUniform("bloomTex", 1);

        _quad->draw();

        glUseProgram(0);

        fb->unbind();
    }

    void drawToScreen(const ShaderProgramPtr& shader, const TexturePtr& inputTexture)
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shader->use();

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        inputTexture->bind();
        shader->setIntUniform("tex", 0);

        glEnable(GL_FRAMEBUFFER_SRGB); //Включает гамма-коррекцию

        _quad->draw();

        glDisable(GL_FRAMEBUFFER_SRGB);

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void drawDebug()
    {
        glClear(GL_DEPTH_BUFFER_BIT);

        int size = 500;

        if (_showGBufferDebug)
        {
            drawQuad(_quadDepthShader, _depthTex, 0, 0, size, size);
            drawQuad(_quadColorShader, _normalsTex, size, 0, size, size);
            drawQuad(_quadColorShader, _diffuseTex, size * 2, 0, size, size);
        }
        else if (_showShadowDebug)
        {
            drawQuad(_quadDepthShader, _shadowTex, 0, 0, size, size);
        }
        else if (_showDeferredDebug)
        {
            drawQuad(_quadColorShader, _deferredTex, 0, 0, size, size);
        }
        else if (_showBloomDebug)
        {
            drawQuad(_quadColorShader, _brightTex, 0, 0, size, size);
            drawQuad(_quadColorShader, _horizBlurTex, size, 0, size, size);
            drawQuad(_quadColorShader, _vertBlurTex, size * 2, 0, size, size);
        }

        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void drawQuad(const ShaderProgramPtr& shader, const TexturePtr& texture, GLint x, GLint y, GLint width, GLint height)
    {
        glViewport(x, y, width, height);

        shader->use();

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        texture->bind();
        shader->setIntUniform("tex", 0);

        _quad->draw();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}